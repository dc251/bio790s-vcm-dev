## Objectives for managed vcm nodes used for Bio790s course. 
* _Enable CentOS SCL repo and EPEL repo._

     >Software Collections (SCL), also known as SCL is a community project that allows you to build, install, and use multiple versions of software on the >same >system, without affecting system default   packages. By enabling SCL you will gain access to the newer versions of programming languages and services >which >are not available in the core repositories.  It will be used to  install newer versions of python 3.x alongside the default python v2.7.5 so that system >tools >such as yum will continue to work properly.

* _Install Python3.6 for CentOS7 using Software Collections (SCL). Use SCL to set Python 3.6 as the default version is the current shell session._

* _Install latest version of R for CentOS7._

* _Install latest version of Nano for CentOS7._

* _Install lynx web browser._

